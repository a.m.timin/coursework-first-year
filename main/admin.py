import imp
from django.contrib import admin
from .models import Recipe
from .models import Porridge
from .models import Pancakes
from .models import Omlet
from .models import Soup
from .models import Sandwich

admin.site.register(Recipe)
admin.site.register(Porridge)
admin.site.register(Pancakes)
admin.site.register(Omlet)
admin.site.register(Soup)
admin.site.register(Sandwich)
# Register your models here.
