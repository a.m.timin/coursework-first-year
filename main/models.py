from django.db import models

# Create your models here.

# Общие рецепты

class Recipe(models.Model):

    CHOICE_INGREDIENTS = (
      ('grain', 'Крупы'),
      ('milk', 'Молоко'),
      ('butter', 'Масло'),
      ('egg', 'Яйца'),
      ('flour', 'Мука'),
    )
    CHOICE_INGREDIENTS5 = (
      ('grain', 'Крупы'),
      ('milk', 'Молоко'),
      ('butter', 'Масло'),
      ('egg', 'Яйца'),
      ('flour', 'Мука'),
      ('mila', 'Милла Йовович'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиенты")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиенты", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиенты", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиенты", blank=True)
    ingedientsFifth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS5, verbose_name="Пятый элемент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингредиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)


# Рецепты каш

class Porridge(models.Model):

    CHOICE_INGREDIENTS_TYPE = (
      ('ris', 'Рис'),
      ('manka', 'Манка'),
      ('grecha', 'Гречка'),
      ('multizlak', 'Мультизерновая'),
      ('ovsyanka', 'Овсянка'),
      ('perlovka', 'Перловка'),
      ('yachnevaya', 'Ячневая'),
      ('kuskus', 'Кус-кус'),
      ('psheno', 'Пшеная'),
      ('kukuruza', 'Кукуруза'),
    )

    CHOICE_INGREDIENTS = (
      ('milk', 'Молоко'),
      ('butter', 'Масло'),
      ('egg', 'Яйца'),
      ('flour', 'Мука'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    type = models.CharField(max_length=50, choices=CHOICE_INGREDIENTS_TYPE, verbose_name="Вид каши", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиент")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиент", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиент", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингридиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)


# Рецепты блинов

class Pancakes(models.Model):

    CHOICE_INGREDIENTS_TYPE = (
      ('sitnie', 'Сытные'),
      ('sladkie', 'Сладкие'),
    )

    CHOICE_INGREDIENTS = (
      ('milk', 'Молоко'),
      ('butter', 'Масло'),
      ('egg', 'Яйца'),
      ('flour', 'Мука'),
      ('vetchina', 'Ветчина'),
      ('kuritsa', 'Курица'),
      ('svinina', 'Свинина'),
      ('indeyka', 'Индейка'),
      ('govyadina', 'Говядина'),
      ('sir', 'Сыр'),
      ('luk', 'Лук'),
      ('tvorog', 'Творог'),
      ('jem', 'Джем'),
      ('zelen', 'Зелень'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    type = models.CharField(max_length=50, choices=CHOICE_INGREDIENTS_TYPE, verbose_name="Вид блинов", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиент")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиент", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиент", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиент", blank=True)
    ingedientsFifth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Пятый ингредиент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингридиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)


# Рецепт омлета

class Omlet(models.Model):

    CHOICE_INGREDIENTS = (
      ('milk', 'Молоко'),
      ('butter', 'Масло'),
      ('egg', 'Яйца'),
      ('flour', 'Мука'),
      ('sir', 'Сыр'),
      ('luk', 'Лук'),
      ('vetchina', 'Ветчина'),
      ('brinza', 'Брынза'),
      ('ovoshi', 'Овощи'),
      ('sosiski', 'Сосиски'),
      ('gribi', 'Грибы'),
      ('parmezano', 'Пармезан'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиент")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиент", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиент", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиент", blank=True)
    ingedientsFifth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Пятый ингредиент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингридиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)


# Рецепт супов

class Soup(models.Model):

    CHOICE_INGREDIENTS = (
      ('milk', 'Молоко'),
      ('lapsha', 'Лапша'),
      ('ris', 'Рис'),
      ('tomat', 'Красный томат'),
      ('sir', 'Сыр'),
      ('luk', 'Лук'),
      ('frukti', 'Фрукты'),
      ('brinza', 'Брынза'),
      ('ovoshi', 'Овощи'),
      ('brokkoli', 'Брокколи'),
      ('gribi', 'Грибы'),
      ('kutitsa', 'Курица'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиент")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиент", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиент", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиент", blank=True)
    ingedientsFifth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Пятый ингредиент", blank=True)
    ingedientsSixth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Шестой ингредиент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингридиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)


# Рецепт сэндвичей

class Sandwich(models.Model):

    CHOICE_INGREDIENTS = (
      ('hleb', 'Хлеб'),
      ('kolbasa', 'Докторская колбаса'),
      ('solami', 'Салями'),
      ('sir', 'Сыр'),
      ('fetaxa', 'Сыр фетакса'),
      ('maslo', 'Масло'),
      ('tomat', 'Томат'),
      ('zelen', 'Зелень'),
      ('tunec', 'Тунец'),
      ('semga', 'Сёмга'),
    )
    
    title = models.CharField(max_length=50, verbose_name="Название рецепта")
    description = models.CharField(max_length=150, verbose_name="Описание рецепта")
    text = models.CharField(max_length=150, verbose_name="Сам рецепт", default='')
    ingedientsFirst = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Первый ингредиент")
    ingedientsSecond = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Второй ингредиент", blank=True)
    ingedientsThrid = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Третий ингредиент", blank=True)
    ingedientsFourth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Четвертый ингредиент", blank=True)
    ingedientsFifth = models.CharField(max_length=15, choices=CHOICE_INGREDIENTS, verbose_name="Пятый ингредиент", blank=True)
    ingedientsExtra = models.CharField(max_length=150, verbose_name="Дополнительные ингридиенты", default='-')
    cost = models.IntegerField(verbose_name="Цена за порцию (руб.)")

    def __str__(self):
      return 'Название: {0}, Цена за порцию: {1} руб.'.format(self.title, self.cost)