from django.urls  import path 
from django.contrib import admin
from . import views 

urlpatterns =  [
  path('', views.home, name= 'home'),
  path('butter/', views.butter, name= 'butter'),
  path('egg/', views.egg, name= 'egg'),
  path('flour/', views.flour, name= 'flour'),
  path('grain/', views.grain, name= 'grain'),
  path('milk/', views.milk, name= 'milk'),
  path('favorite/', views.favorite, name= 'favorite'),
  path('admin/', admin.site.urls,)
  # path('admin/', admin.site.urls, name= 'admin')
]